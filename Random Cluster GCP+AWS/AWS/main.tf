
#Setup the backend
#I know that I can provide values for bucket with something like -backend-config=backend.tfvars, but in my case that would be redundant.

#terraform {
#  backend "s3" {
    # Replace this with your bucket name!
#    bucket         = "terraform_state_bucket"
#    key            = "global/s3/terraform.tfstate"
#    region         = "us-east-1"

    # Replace this with your DynamoDB table name!
#    dynamodb_table = "terraform_lock_state_DB"
#    encrypt        = true
#  }
#}
### Commented for time being ###

#Check available AWS zones
data "aws_availability_zones" "available" {
  state = "available"
}

#Shuffle available AWS zones, get 3 results (Possible to set up more, according to available azs)
resource "random_shuffle" "aws_az" {
  input = data.aws_availability_zones.available.names
  result_count = 3
}

#Call VPC module and set the Networking values
module "vpc" {
  source = "./modules/vpc"

  networking = {
  cidr_block = "172.16.16.0/20"
  region = var.region
  vpc_name = "aws_terraform_vpc"
  aws_az = ["${element(random_shuffle.aws_az.result, 0)}", "${element(random_shuffle.aws_az.result, 1)}", "${element(random_shuffle.aws_az.result, 2)}"]
  public_subnets = ["172.16.16.128/25", "172.16.17.128/25", "172.16.18.128/25"]
  private_subnets = ["172.16.16.0/25", "172.16.17.0/25", "172.16.18.0/25"]
  }
}

#Call s_g module and pass values
module "security_group" {
  source = "./modules/security_group"

  vpc_id = module.vpc.vpc_id
  cidr_block = module.vpc.cidr_block
}

# Call for the available subnets in the region
data "aws_subnets" "available_subnets" {
}

#Call nodes module and pass values
module "aws_nodes" {
  source = "./modules/nodes"
  for_each      = toset(data.aws_subnets.available_subnets.ids)
  security_group_id = module.security_group.security_group_id
  subnet_id = each.value
}

#I use the *toset* function to convert the list of subnet IDs into a set.
#This might be beneficial if you ever need functionality specific to sets in the loop
#I know that using the list directly might be easier, and this approach here is basically useless.
#Yet, who knows what I'd like to add here next, so I'll keep it at that.