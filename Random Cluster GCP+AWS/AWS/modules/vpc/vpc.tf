
#Configure AWS VPC
resource "aws_vpc" "create_vpc" {
 cidr_block = var.networking.cidr_block

 tags = {
   Name = var.networking.vpc_name
 }
}

# Set Up Public Subnet
resource "aws_subnet" "public_subnets" {
  count = var.networking.public_subnets == null || var.networking.public_subnets == "" ? 0 : length(var.networking.public_subnets) #Explanation Below
  vpc_id = aws_vpc.create_vpc.id
  cidr_block = var.networking.public_subnets[count.index]
  availability_zone = var.networking.aws_az[count.index]
  map_public_ip_on_launch = "true"

  tags = {
    Name = "my_public_subnet-${count.index}"
  }
}

#Private Subnets
resource "aws_subnet" "private_subnets" {
  count = var.networking.private_subnets == null || var.networking.private_subnets == "" ? 0 : length(var.networking.private_subnets) #Explanation Below
  vpc_id = aws_vpc.create_vpc.id
  cidr_block = var.networking.private_subnets[count.index]
  availability_zone = var.networking.aws_az[count.index]
  map_public_ip_on_launch = "false"

  tags = {
    Name = "my_private_subnet-${count.index}"
  }
}

#Set up Gateway
resource "aws_internet_gateway" "keeper" {
 vpc_id = aws_vpc.create_vpc.id

 tags = {
  Name = "Keeper"
 }
}
#Create NAT Gateway
resource "aws_nat_gateway" "nat_routing" {
  subnet_id     = aws_subnet.public_subnets[0].id #Choosing the first created public subnet to place a gateway

  tags = {
    Name = "NAT-gateway"
  }
  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.keeper]
}

#Create Public and Private route tables
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.create_vpc.id

  route {
    cidr_block           = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.keeper.id
  }
  tags = {
    Name = "public_route_table"
  }
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.create_vpc.id
 route {
   cidr_block           = "0.0.0.0/0"
   nat_gateway_id = aws_nat_gateway.nat_routing.id
  }
  tags = {
    Name = "private_route_table"
  }
}

resource "aws_route_table_association" "associate_public" {
  count = var.networking.public_subnets == null || var.networking.public_subnets == "" ? 0 : length(var.networking.public_subnets)
  subnet_id = aws_subnet.public_subnets[count.index].id #access ID by using count
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table_association" "associate_private" {
  count = var.networking.private_subnets == null || var.networking.private_subnets == "" ? 0 : length(var.networking.private_subnets)
  subnet_id = aws_subnet.private_subnets[count.index].id #access ID by using count
  route_table_id = aws_route_table.private.id
}


#This line of Terraform code is a conditional expression that determines the count for a resource based on the availability of public subnets in NETWORKING variable.
# - count: Is a meta-argument in Terraform that specifies how many times a resource should be created.
# - var.networking.public_subnets: This accesses the public_subnets list within the networking variable, which presumably contains CIDR blocks for public subnets.
# - == null: checks if the public_subnets list is null (i.e., has no value assigned).
# - == "": checks if the public_subnets list is an empty string ("").
# - ? 0: This is the "if true" part of the conditional expression. If public_subnets is null or an empty string, the count will be set to 0.
# - : length(var.networking.public_subnets): is the "if false" part of the expression. If public_subnets is a valid list with values, the count will be set to the length of the list (number of subnets).