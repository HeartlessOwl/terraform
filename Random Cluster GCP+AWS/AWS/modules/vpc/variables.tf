variable "networking" {
  type = object({
    cidr_block = string
    region = string
    vpc_name = string
    aws_az = list(string)
    public_subnets = list(string)
    private_subnets = list(string)
#    nat_gateways = bool
  })

  default = {
    cidr_block      = "10.0.0.0/16"
    region          = "us-east-1"
    vpc_name        = "custom-vpc"
    aws_az             = ["us-east-1a", "us-east-1b"]
    public_subnets  = ["10.0.1.0/24", "10.0.2.0/24"]
    private_subnets = ["10.0.3.0/24", "10.0.4.0/24"]
    nat_gateways    = true
  }

}