output "vpc_id" {
  value = aws_vpc.create_vpc.id
}

output "cidr_block" {
  value = aws_vpc.create_vpc.cidr_block
}

output "public_subnets_id" {
  value = [aws_subnet.public_subnets[*].id]
}

output "private_subnets_id" {
  value = [aws_subnet.private_subnets[*].id]
}

