#variable "vpc_id" {
#  type = string
#}

variable "security_group_id" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "ami_id" {
  type = string
  default = "ami-0d729a60"
}