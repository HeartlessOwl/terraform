# Get all available AWS zones
#data "aws_availability_zones" "available" {
#  state = "available"
#}

# Declare aws zone shuffle
#resource "random_shuffle" "aws_az" {
#  input = data.aws_availability_zones.available.names
#  result_count = 5
#}

# AWS Instances in random availability zones
resource "random_id" "node" {
  keepers = {
    # Generate a new id each time we switch to a new AMI id
    ami_id = var.ami_id
  }

  byte_length = 8
}


resource "aws_instance" "aws_random_cluster" {
  ami = random_id.node.keepers.ami_id
  instance_type = "t2.micro"
  subnet_id = var.subnet_id
  tags = {
    name = "node-${random_id.node.hex}"
  }
}


