

#Create bucket, prevent deletion
resource "aws_s3_bucket" "terraform_state" {
  bucket = var.tf_state_bucket
  lifecycle {
    prevent_destroy = true
  }
}

#Enable Versioning
resource "aws_s3_bucket_versioning" "enable_versioning" {
  bucket = aws_s3_bucket.terraform_state.id
  versioning_configuration {
    status = "Enabled"
  }
}

#Enable Server-side Encryption
resource "aws_s3_bucket_server_side_encryption_configuration" "enable_encryption" {
  bucket = aws_s3_bucket.terraform_state.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

#Block Public Access
resource "aws_s3_bucket_public_access_block" "block_public_access" {
  bucket                  = aws_s3_bucket.terraform_state.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

#Create DynamoDB for the Terraform-State-lock
resource "aws_dynamodb_table" "terraform_locks" {
  name         = var.tf_state_lock
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}