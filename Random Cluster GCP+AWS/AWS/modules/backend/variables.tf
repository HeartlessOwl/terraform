variable "tf_state_bucket" {
  type = string
  default = "terraform-state-bucket"
}

variable "tf_state_lock" {
  type = string
  default = "terraform-lock"
}

