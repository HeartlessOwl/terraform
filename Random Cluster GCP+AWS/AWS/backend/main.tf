#Used once to setup the backend for main project

provider "aws" {
  region = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}

module "backend" {
  source = "../modules/backend"
  tf_state_bucket = "test_state_bucket" #Will Replace with variable
  tf_state_lock = "test_lock_db" #Will Replace with variable
}