<h1> To Do: </h1>

1. Finish the VPC module (transfer values to variables and set up AZ's) - *DONE*
2. Set up the Remote state file in S3 - *DONE* (backend module can be called inside the backend folder of any project, runs only once)
3. Understand how to handle randomness in the task - *DONE*
4. Upgrade the Security Group module *TO DO*
5. Add Random ID namings!!! *DONE*
6. Upgrades to VPC - Output values, Error Handling (Route tables, and NAT gateways also needed) *IN PROGRESS*
7. Configure a NACLs module *TO DO*
8. Broaden Security and Compliance (IAM Roles/Users/Policies) *TO DO*
9. CloudWatch alarms module *TO DO*
10. Terraform state management module *TO DO*
11. Learn more about module testing *TO DO*



