# Changelog

All notable changes to this project will be documented in this file.

Maintainer: Chag Deniz Daniel [ddchag@gmail.com]

Template:

## [Unreleased]

### External API changes
N/A

### Internal API changes
N/A

### Additions
N/A

### Bug fixes
N/A

### Upgrade actions
N/A

---


## [0.1.3] March 26th 2024

### External API changes
N/A

### Internal API changes
VPC module greatly upgraded:
 - NAT Gateway
 - Public/Private Routing Tables
 - Mapping between Subnets and Routing Tables

### Additions
Introduced changelog

### Bug fixes
N/A

### Upgrade actions
N/A

---


## [0.1.21] March 25th 2024

### External API changes
N/A

### Internal API changes
N/A

### Additions
Random ID generation for nodes

### Bug fixes
N/A

### Upgrade actions
N/A

---

## [0.1.2] March 25th 2024

### External API changes
N/A

### Internal API changes
Backend module introduced

### Additions
Readme updated with future plans

### Bug fixes
N/A

### Upgrade actions
N/A

---

## [0.1.1] March 14th 2024

### External API changes
N/A

### Internal API changes
Restructured VPC module
Introduces random provider for several variables

### Additions
N/A

### Bug fixes
N/A

### Upgrade actions
N/A

---



## [0.1.0] March 13th 2024

### External API changes
N/A

### Internal API changes
Project created
Introduced modules:
    -Nodes
    -Security_Group

### Additions
N/A

### Bug fixes
N/A

### Upgrade actions
N/A

---
