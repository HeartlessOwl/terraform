#5 GCP Instances in random zones
resource "google_compute_instance" "gcp_instance" {
  count = 5
  name         = "firstborn"
  machine_type = "f1-micro"
  zone         = "us-central1-a"

  tags = ["terraform-machine"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"
    access_config {
      // Ephemeral public IP
    }
  }
}

