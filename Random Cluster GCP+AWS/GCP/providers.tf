# Configure GCP provider
provider "google" {
  project     = "gcp-keys.json"
  region      = "us-central1"
  zone        = "us-central1-c"
  credentials = "gcp-keys.json"
}
